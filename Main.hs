{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE FlexibleContexts, BangPatterns #-}

import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import Data.Array.Repa (Array, U, Z(..), (:.)(..), DIM1, DIM2)
import qualified Data.Array.Repa as R 
import Data.Array.Repa.Repr.ForeignPtr (F)
import qualified Data.Array.Repa.Repr.ForeignPtr as R

import Graphics.Gloss.Juicy
import Codec.Picture

import GHC.Float 
import qualified Data.ByteString as B 

import Debug.Trace
import Data.Word


-- import System.IO.Unsafe

data World = World (Array F DIM1 Word8) !(Array U DIM2 Double) !(Array U DIM2 Double)

maxI = 400
maxJ = 400

speed     = 0.1
reduction = 0.9998 
scaleXY   = 0.001 
scaleTime = 0.1 

{-

u(x,y,t+dt) =
 c^2 (dt/dx)^2 (u(x-dx,y,t) + u(x+dx,y,t) + u(x,y-dy,t) + u(x,y+dy,t)
  - 4*u(x,y))
 + (u(x,y,t-dt) - 2 * u(x,t))
-}
step :: Double -> Float -> World -> World 
step !dx !dtF (World _ arr1 arr2) =
  let arr1' = R.computeS $ R.map (* reduction) $ R.traverse2 arr1 arr2 (\a _ -> a)
             (\f g (Z :. i :. j) ->
               if 0 < i && i < maxI && 0 < j && j < maxJ then
                 a * (f (Z :. i-1 :. j) + f (Z :. i+1 :. j) + f (Z :. i :. j-1)
                      + f (Z :. i :. j+1) - 4*f (Z :. i :. j))
                 - (g (Z :. i :. j) - 2 * f (Z :. i :. j))
               else
                 0)
  in World (renderToBuffer arr1') arr1' arr1
  where
    !dt = float2Double dtF 
    !a  = speed^2 * (dt^2/dx^2)

r2c v | redColor < 0   = PixelRGB8 0            lo  lo
      | redColor > 255 = PixelRGB8 255          hi  hi
      | otherwise      = PixelRGB8 (f redColor) 140 140 
  where redColor = floor $ 127 * v + 128 
        lo = f $ max 0   $ 140 + redColor
        hi = f $ min 255 $ 140 + redColor - 255
        f  = fromIntegral 

render :: World -> Picture      
render (World buf _ _) =
  let ptr = R.toForeignPtr buf
  in bitmapOfForeignPtr maxJ maxI (BitmapFormat BottomToTop PxABGR) ptr False 

renderToBuffer :: Array U DIM2 Double -> Array F DIM1 Word8
renderToBuffer arr = 
  R.computeS $ R.traverse arr (const $ Z :. (maxJ * maxI * 4)) $ \f (Z :. ix) -> 
     let (ix', c) = divMod ix 4
         (  i, j) = divMod ix' maxI 
     in color (f (Z :. i :. j)) c
  where
    {-# INLINE color #-}
    color :: Double -> Int -> Word8
    color r n = 
      case n of 
        0 -> 255
        1 -> blue
        2 -> green
        3 -> red
      where rawRed = floor $ 127 * r + 128
            red | rawRed < 0   = 0
                | rawRed > 255 = 255
                | otherwise    = fromIntegral rawRed 
            blue | rawRed < 0   = fromIntegral $ max 0 $ 140 + rawRed 
                 | rawRed > 255 = fromIntegral $ min 255 $ 140 + rawRed - 255
                 | otherwise    = 140 
            green = blue
    
       

-- render (World arr _) = 
--   bitmapOfByteString maxJ maxI 
--   (fst $ B.unfoldrN (maxJ * maxI * 4) (\ix -> Just (f ix, ix+1)) 0) False
--   where
--     f ix = let (ix', c) = divMod ix 4
--                (  i, j) = divMod ix' maxI 
--            in color (R.index arr (Z :. i :. j)) c 

--     color r n = 
--       case n of 
--         0 -> 255
--         1 -> blue
--         2 -> green
--         3 -> red
--       where rawRed = floor $ 127 * r + 128
--             red | rawRed < 0   = 0
--                 | rawRed > 255 = 255
--                 | otherwise    = fromIntegral rawRed 
--             blue | rawRed < 0   = fromIntegral $ max 0 $ 140 + rawRed 
--                  | rawRed > 255 = fromIntegral $ min 255 $ 140 + rawRed - 255
--                  | otherwise    = 140 
--             green = blue
              

-- render (World arr _) = 
--   fromImageRGB8 $ generateImage 
--     (\x y -> r2c (R.index arr (Z :. invertI y :. x))) maxJ maxI 
    
-- invertI !i = maxI - i 

eventHandler :: Event -> World -> World
eventHandler (EventKey (MouseButton LeftButton) Down _ (!x,!y)) (World r a1 a2) = 
  let !a1' = replace a1 
  in World (renderToBuffer a1') a1' a2
  where
    transX x = (round x + maxJ `div` 2) 
    transY y = (round y + maxI `div` 2) 
    replace a = R.computeS $ R.traverse a id
                    (\f (ix@(Z :. i :. j)) ->
                      if transX x == j && transY y == i then 
                        1
                      else
                        f ix)
eventHandler e w = w

initArr :: Array U (Z :. Int :. Int) Double
initArr = R.computeS $ R.fromFunction (Z :. maxI + 1 :. maxJ + 1) (\_ -> 0)
     
main = play (InWindow "Ripple" (maxI, maxJ) (80, 80)) 
            black
            60
            (World (renderToBuffer initArr) initArr initArr)
            render
            eventHandler
            (\dt -> step scaleXY (scaleTime * dt))
            
       
